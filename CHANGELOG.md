#[0.2.0] - 01-08-2021
    - Improved custom inspector.
    - Now when you change target the old methods go away.

#[0.1.0] - 01-05-2021
    - Now its a package.
