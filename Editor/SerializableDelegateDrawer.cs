﻿using System;
using System.Collections.Generic;
using System.Reflection;
using UnityEditor;
using UnityEngine;

namespace Tinnuts.SerializableDelegates
{
	//XXX: Unity do not support custom drawers from generic classes so this is needed to bypass that problem.
	//CUSTOM PROPERTY DRAWERS CHILDS
	[CustomPropertyDrawer(typeof(SD_void))] public class SD_voidDrawer : SerializableDelegateDrawer<Action> { }
	[CustomPropertyDrawer(typeof(SD_void_float))] public class SD_void_floatDrawer : SerializableDelegateDrawer<Action<float>> { }
	[CustomPropertyDrawer(typeof(SD_bool_bool))] public class SD_bool_boolDrawer : SerializableDelegateDrawer<Func<bool,bool>> { }
	[CustomPropertyDrawer(typeof(SD_bool))] public class SD_boolDrawer : SerializableDelegateDrawer<Func<bool>> { }
	//CUSTOM PROPERTY DRAWERS CHILDS END

	public class SerializableDelegateDrawer<T> : PropertyDrawer
	{
		public override float GetPropertyHeight (SerializedProperty property, GUIContent label)
		{
			SerializedProperty entriesProperty = property.FindPropertyRelative("m_Entries");

			float entriesHeight = 0f;
			for (int i = 0; i < entriesProperty.arraySize; i++)
			{
				entriesHeight += EditorGUI.GetPropertyHeight(entriesProperty.GetArrayElementAtIndex(i));
			}

			// All entries heights + label height + button height
			float totalHeight = entriesHeight + (2 * EditorGUIUtility.singleLineHeight);
			return totalHeight;
		}

		public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
		{
			EditorGUI.BeginProperty(position, label, property);

			// Get delegate return and parameters types through the invoke method using reflection.
			MethodInfo invokeInfo = typeof(T).GetMethod("Invoke");	
			string returnType = invokeInfo.ReturnType.ToString();
			ParameterInfo[] paramsInfo = invokeInfo.GetParameters();
			List<string> paramsType = new List<string>();
			foreach (var paramInfo in paramsInfo)
			{
				paramsType.Add(paramInfo.ParameterType.ToString());
			}

			EditorGUI.PrefixLabel(position, GUIUtility.GetControlID(FocusType.Passive), new GUIContent(label.text + " (" + returnType + ")" + " (" + string.Join(", ", paramsType) + ")"));
			position.y += EditorGUIUtility.singleLineHeight;

			int iLevel = EditorGUI.indentLevel;
			EditorGUI.indentLevel = 0;

			float deleteWidth = position.width * 0.07f;
			float entryWidth = position.width * 0.93f;

			SerializedProperty entriesProperty = property.FindPropertyRelative("m_Entries");

			int markForDelete = -1;	

			for (int i = 0; i < entriesProperty.arraySize; i++)
			{
				SerializedProperty entryProperty = entriesProperty.GetArrayElementAtIndex(i);

				var height = EditorGUI.GetPropertyHeight(entryProperty, GUIContent.none);

				Rect deleteRect = new Rect(position.x, position.y, deleteWidth, height);
				if (GUI.Button(deleteRect, "x"))
				{
					markForDelete = i;
				}

				Rect entryRect = new Rect(position.x + deleteWidth, position.y, entryWidth, height);
				position.y += height;

				EditorGUI.PropertyField(entryRect, entryProperty, GUIContent.none);
			}

			if (markForDelete != -1)
			{
				entriesProperty.DeleteArrayElementAtIndex(markForDelete);
				markForDelete = -1;
			}

			Rect buttonRect = new Rect(position.x, position.y, position.width, EditorGUIUtility.singleLineHeight);

			if (GUI.Button(buttonRect, "Add Target"))
			{	
				// Create new entry, clear fields and set return type and parameters type to match on the inspector.
				entriesProperty.InsertArrayElementAtIndex(entriesProperty.arraySize);
				SerializedProperty newEntry = entriesProperty.GetArrayElementAtIndex(entriesProperty.arraySize-1);
				newEntry.FindPropertyRelative("_object").objectReferenceValue = null;
				newEntry.FindPropertyRelative("_methods").ClearArray();
				newEntry.FindPropertyRelative("m_ReturnType").stringValue = returnType;
				SerializedProperty newEntryParamsProperty = newEntry.FindPropertyRelative("m_ParamsTypes");
				newEntryParamsProperty.ClearArray();
				for (int i = 0; i < paramsInfo.Length; i++)
				{
					newEntryParamsProperty.InsertArrayElementAtIndex(i);
					SerializedProperty paramProperty = newEntryParamsProperty.GetArrayElementAtIndex(i);
					paramProperty.stringValue = paramsInfo[i].ParameterType.ToString();
				}
			}

			EditorGUI.indentLevel = iLevel;

			EditorGUI.EndProperty();
		}
	}
}
