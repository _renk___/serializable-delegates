﻿using System;
using System.Reflection;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace Tinnuts.SerializableDelegates
{
	[CustomPropertyDrawer(typeof(Entry))]
	public class EntryPropertyDrawer : PropertyDrawer
	{
		public struct PopupMethodData
		{
			public SerializedProperty methodsProperty;
			public string assemblyQualifiedName;
			public string methodName;

			public PopupMethodData(SerializedProperty methodsProperty, string assemblyQualifiedName,  string methodName)
			{
				this.methodsProperty = methodsProperty;
				this.assemblyQualifiedName = assemblyQualifiedName;
				this.methodName = methodName;
			}
		}

		public override float GetPropertyHeight (SerializedProperty property, GUIContent label)
		{
			float methodsCount = property.FindPropertyRelative("_methods").arraySize;
			if (methodsCount == 0) methodsCount = 1;
			return (EditorGUIUtility.singleLineHeight + EditorGUIUtility.standardVerticalSpacing) * (methodsCount);
		}

		public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
		{
			EditorGUI.BeginProperty(position, label, property);

			float halfWidth = position.width / 2f;
			float propertyWidth = halfWidth * 0.87f;
			float buttonWidth = halfWidth * 0.13f;

			SerializedProperty targetProperty = property.FindPropertyRelative("_object");
			float height = EditorGUI.GetPropertyHeight(targetProperty, GUIContent.none);
			Rect targetRect = new Rect(position.x, position.y, propertyWidth, height);

			EditorGUI.BeginChangeCheck();
			EditorGUI.PropertyField(targetRect, targetProperty, GUIContent.none);
			if (EditorGUI.EndChangeCheck())
			{
				property.FindPropertyRelative("_methods").ClearArray();
			}
			
			SerializedProperty methodsProperty = property.FindPropertyRelative("_methods");
			int count = methodsProperty.arraySize;

			List<(string, string)> currentMethods = new List<(string, string)>();

			for (int i = 0; i < count; i++)
			{
				SerializedProperty typeMethodPairProperty = methodsProperty.GetArrayElementAtIndex(i);
				currentMethods.Add(
					(typeMethodPairProperty.FindPropertyRelative("assemblyQualifiedName").stringValue,
					 typeMethodPairProperty.FindPropertyRelative("method").stringValue)
				);
			}

			string returnType = property.FindPropertyRelative("m_ReturnType").stringValue;
			SerializedProperty paramsTypesProperty = property.FindPropertyRelative("m_ParamsTypes");
			List<string> tempParamsTypes = new List<string>();
			for (int i = 0; i < paramsTypesProperty.arraySize; i++)
			{
				tempParamsTypes.Add(paramsTypesProperty.GetArrayElementAtIndex(i).stringValue);
			}

			Rect plusRect = new Rect(position.x + propertyWidth, position.y, buttonWidth, height);
			if (GUI.Button(plusRect, "+"))
			{
				GenericMenu popup = new GenericMenu();
				(string, string)[] options = GetMethodNames(targetProperty, returnType, tempParamsTypes.ToArray());

				popup.AddDisabledItem(new GUIContent("(" + returnType + ")" + " (" + string.Join(", ", tempParamsTypes) + ")"));
				
				foreach (var option in options)
				{
					if (!currentMethods.Contains(option))
					{
						PopupMethodData data = new PopupMethodData(methodsProperty, option.Item1, option.Item2); 
						popup.AddItem(new GUIContent(option.Item1.Split(',')[0] + "/" + option.Item2), false, PopupCallback, data);
					}
				}

				if (popup.GetItemCount() == 2)
				{
					popup.AddDisabledItem(new GUIContent("No more matching methods were found."));
				}

				popup.ShowAsContext();
			}

			int iLevel = EditorGUI.indentLevel;
			EditorGUI.indentLevel = 0;

			int markForDelete = -1;

			for(int i = 0; i < methodsProperty.arraySize; i++)
			{
				float positionY = position.y + ((EditorGUIUtility.singleLineHeight + EditorGUIUtility.standardVerticalSpacing) * i);

				SerializedProperty typeMethodPairProperty = methodsProperty.GetArrayElementAtIndex(i);
				SerializedProperty methodProperty = typeMethodPairProperty.FindPropertyRelative("method");
				height = EditorGUI.GetPropertyHeight(methodProperty, GUIContent.none);
				Rect methodRect = new Rect(position.x + halfWidth, positionY, propertyWidth, height);
				EditorGUI.LabelField(methodRect, methodProperty.stringValue);

				Rect minusRect = new Rect(position.x + halfWidth + propertyWidth, positionY, buttonWidth, height);
				if (GUI.Button(minusRect, "-"))
				{
					markForDelete = i;
				}
			}

			if (markForDelete != -1)
			{
				methodsProperty.DeleteArrayElementAtIndex(markForDelete);
			}

			EditorGUI.indentLevel = iLevel;

			EditorGUI.EndProperty();
		}

		private (string, string)[] GetMethodNames(SerializedProperty target, string returnType, string[] paramsTypes)
		{
			List<(string, string)> methodNames = new List<(string, string)>();

			dynamic targetObject = Convert.ChangeType(target.objectReferenceValue, target.objectReferenceValue.GetType());

			if (targetObject != null)
			{
				Component[] components = targetObject.GetComponents<Component>();
				foreach (var component in components)
				{
					MethodInfo[] methods = component.GetType().GetMethods(BindingFlags.Public|BindingFlags.Instance|BindingFlags.DeclaredOnly);

					if (returnType == null || returnType == "" || paramsTypes == null)
						return methodNames.ToArray();

					foreach (var method in methods)
					{
						ParameterInfo[] paramsInfo = method.GetParameters();

						if (method.ReturnType.ToString().CompareTo(returnType) != 0)
							continue;

						if (!CompareParams(paramsInfo, paramsTypes))
							continue;

						methodNames.Add((component.GetType().AssemblyQualifiedName, method.Name));
					}
				}
			}

			return methodNames.ToArray();
		}

		///<summary>
		///Return true if the count and type of the parameters match.
		///</summary>
		private bool CompareParams(ParameterInfo[] paramsInfo, string[] paramsTypes)
		{
			if (paramsInfo.Length != paramsTypes.Length)
				return false;

			for (int i = 0; i < paramsInfo.Length; i++)
			{
				if (paramsInfo[i].ParameterType.ToString().CompareTo(paramsTypes[i]) != 0)
					return false;
			}

			return true;
		}

		private void PopupCallback(object obj)
		{
			PopupMethodData data = (PopupMethodData)obj;

			data.methodsProperty.serializedObject.Update();

			data.methodsProperty.InsertArrayElementAtIndex(data.methodsProperty.arraySize);
			SerializedProperty newTypeMethodPair = data.methodsProperty.GetArrayElementAtIndex(data.methodsProperty.arraySize - 1);
			newTypeMethodPair.FindPropertyRelative("assemblyQualifiedName").stringValue = data.assemblyQualifiedName;
			newTypeMethodPair.FindPropertyRelative("method").stringValue = data.methodName;

			data.methodsProperty.serializedObject.ApplyModifiedProperties();
		}
	}
}
