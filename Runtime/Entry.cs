﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Tinnuts.SerializableDelegates
{
	[Serializable]
	public class TypeMethodPair
	{
		public string assemblyQualifiedName;
		public string method;

		public TypeMethodPair(string assemblyQualifiedName, string method)
		{
			this.assemblyQualifiedName = assemblyQualifiedName;
			this.method = method;
		}
	}
	
	[Serializable]
	public class Entry
	{
		public GameObject _object;
		public List<TypeMethodPair> _methods = new List<TypeMethodPair>();
		
		[SerializeField] private string m_ReturnType;
		[SerializeField] private string[] m_ParamsTypes;

		public Entry() { }
		public Entry(GameObject target, TypeMethodPair method)
		{
			_object = target;
			_methods.Add(method);
		}
	}
}
