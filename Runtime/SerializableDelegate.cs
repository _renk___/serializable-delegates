﻿using System;
using System.Reflection;
using System.Collections.Generic;
using UnityEngine;

namespace Tinnuts.SerializableDelegates
{
	[Serializable] public class SD_void : SerializableDelegate<Action> {};
	[Serializable] public class SD_void_float : SerializableDelegate<Action<float>> {};
	[Serializable] public class SD_bool : SerializableDelegate<Func<bool>> {};
	[Serializable] public class SD_bool_bool : SerializableDelegate<Func<bool, bool>> {};

	[Serializable]
	public class SerializableDelegate<T> where T : MulticastDelegate
	{
		public T action => RebuildDelegate();

		[SerializeField]
		private List<Entry> m_Entries = new List<Entry>();
		private T m_Action;

		public static SerializableDelegate<T> operator + (SerializableDelegate<T> s, T a)
		{
			if (a == null || (MonoBehaviour)a.Target == null)
				return s;

			GameObject target = (GameObject)a.Target;
			Entry entry = s.m_Entries.Find((Entry e) => { return e._object == target; });
			if (entry != null)
			{
				entry._methods.Add(new TypeMethodPair(a.Target.GetType().AssemblyQualifiedName, a.Method.Name));
			}
			else
			{
				s.m_Entries.Add(new Entry(target, new TypeMethodPair(a.Target.GetType().AssemblyQualifiedName, a.Method.Name)));
			}

			s.m_Action = (T)Delegate.Combine(s.RebuildDelegate(), a);
			return s;
		}

		public static SerializableDelegate<T> operator - (SerializableDelegate<T> s, T a)
		{
			if (a == null || (MonoBehaviour)a.Target == null)
				return s;
			
			GameObject target = (GameObject)a.Target;
			Entry entry = s.m_Entries.Find((Entry e) => { return e._object == target; });
			if (entry != null)
			{
				entry._methods.Remove(new TypeMethodPair(a.Target.GetType().AssemblyQualifiedName, a.Method.Name));
				if (entry._methods.Count == 0)
				{
					s.m_Entries.Remove(entry);
				}
			}

			s.m_Action = (T)Delegate.Remove(s.RebuildDelegate(), a);
			return s;
		}

		private T RebuildDelegate()
		{
			if (m_Action == null)
			{
				foreach (var entry in m_Entries) 
				{
					UnityEngine.Object target = entry._object;
					if (target == null)
						continue;
					
					foreach (var typeMethodPair in entry._methods)
					{
						//TODO: add support to non components methods.
						Component component = entry._object.GetComponent(Type.GetType(typeMethodPair.assemblyQualifiedName));
						if (component != null)
						{
							target = component;
						}
						
						T newDelegate = (T)Delegate.CreateDelegate(typeof(T), target, typeMethodPair.method);
						m_Action = (T)Delegate.Combine(m_Action, newDelegate);
					}
				}
			}

			return m_Action;
		}
	}
}
